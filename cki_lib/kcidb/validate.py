"""CKI KCIDB Schema."""
import argparse
import json
import pathlib
import pkgutil

import jsonschema
from kcidb_io.schema import V4 as KCIDB_SCHEMA
import yaml

from cki_lib.logger import get_logger

LOGGER = get_logger(__name__)

SCHEMA = yaml.safe_load(pkgutil.get_data(__name__, 'schema.yaml'))['schema']

KCIDB_STATUS_CHOICES = KCIDB_SCHEMA.json['$defs']['test']['properties']['status']['enum']


def sanitize_kcidb_status(result: str) -> str:
    """Sanitize the result from tests and subtests.

    Valid KCIDB status: FAIL, ERROR, PASS, DONE, SKIP
    Valid BEAKER/RESTRAINT results: Fail, Warn, WARN/ABORTED, Panic, Pass, Skip

    - Beaker/Restraint results that match a KCIDB status are equivalent;
    - "Warn", "WARN/ABORTED" and "Panic" are parsed as FAIL;
    - Anything other than that is parsed as ERROR.
    """
    status = result.upper()
    if status in ('WARN', 'PANIC', 'WARN/ABORTED'):
        return "FAIL"

    if status not in KCIDB_STATUS_CHOICES:
        LOGGER.warning("Parsing invalid status %r into ERROR", status)
        return "ERROR"

    return status


def validate_kcidb(data):
    """Validate kcidb with additional checks."""
    KCIDB_SCHEMA.validate(data)

    # Check if origin is in checkout id
    for checkout in data.get('checkouts', []):
        if not checkout['id'].startswith(checkout['origin'] + ':'):
            raise jsonschema.exceptions.ValidationError(
                f"checkout id ({checkout['id']}) does not match "
                f"origin ({checkout['origin']}) constraint"
            )


def validate(data):
    """Validate data against CKI and KCIDB formats."""
    validate_kcidb(data)
    jsonschema.validate(
        instance=data,
        schema=SCHEMA,
        format_checker=jsonschema.Draft7Validator.FORMAT_CHECKER
    )


def main(argv=None):
    """Run the command line interface."""
    parser = argparse.ArgumentParser()
    parser.add_argument('file')

    args = parser.parse_args(argv)

    validate(json.loads(pathlib.Path(args.file).read_text(encoding='utf8')))


if __name__ == "__main__":
    main()
