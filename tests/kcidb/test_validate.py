import unittest

import jsonschema

from cki_lib.kcidb.validate import validate_kcidb


class TestValidateKCIDB(unittest.TestCase):
    """Test validate_kcidb."""

    def test_origin(self):
        """Test origin validation."""
        test_data = (
            ('Valid data',
             [{'id': 'foo:1', 'origin': 'foo'}, {'id': 'bar:1', 'origin': 'bar'}], True),
            ('Missing origin',
             [{'id': 'foo:1', 'origin': 'foo'}, {'id': 'bar:1'}], False),
            ('Invalid origin',
             [{'id': 'foo:1', 'origin': 'bar'}, {'id': 'bar:1', 'origin': 'foo'}], False),
        )

        for description, checkouts, valid in test_data:
            with self.subTest(description):
                data = {'version': {'major': 4, 'minor': 0}, 'checkouts': checkouts}
                if valid:
                    validate_kcidb(data)
                else:
                    self.assertRaises(jsonschema.exceptions.ValidationError, validate_kcidb, data)
